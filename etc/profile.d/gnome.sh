#!/usr/bin/env bash

# Fix cursor bug in GNOME XORG
if [ "$DESKTOP_SESSION" == "gnome-xorg" ]; then
  xsetroot -cursor_name left_ptr
fi

# QT settings (for GNOME and Phosh)
if [[ "$XDG_CURRENT_DESKTOP" == "GNOME" ]]; then
  export QT_AUTO_SCREEN_SCALE_FACTOR=1
  export QT_QPA_PLATFORMTHEME=qt5ct
elif [[ "$XDG_CURRENT_DESKTOP" == "Phosh:GNOME" ]] ; then
	export QT_QPA_PLATFORMTHEME=qt5ct
fi
