#!/bin/sh
dconf load / < ~/.config/lunaos-dconf.ini

#create a empty file in template directory
if locale | grep "LANG=ru_RU.UTF-8" ; then
	touch $(xdg-user-dir TEMPLATES)/Новый\ Bash\ Скрипт
	touch $(xdg-user-dir TEMPLATES)/Новый\ Файл
	touch $(xdg-user-dir TEMPLATES)/Новый\ Текстовый\ Файл.txt
	echo "#!/bin/bash" > $(xdg-user-dir TEMPLATES)/Новый\ Bash\ Скрипт
else
	touch $(xdg-user-dir TEMPLATES)/Empty\ File
	touch $(xdg-user-dir TEMPLATES)/Empty\ File
	touch $(xdg-user-dir TEMPLATES)/Empty\ Text\ File.txt
	echo "#!/usr/bin/env bash" > $(xdg-user-dir TEMPLATES)/Empty\ Bash
fi

rm -rf ~/.config/lunaos-dconf.ini &
rm -rf ~/.config/autostart-scripts/dconf.sh &

notify-send "GNOME settings applied!"
